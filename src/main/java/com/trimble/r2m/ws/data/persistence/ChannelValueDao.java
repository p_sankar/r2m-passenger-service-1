package com.trimble.r2m.ws.data.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import com.google.inject.Inject;
import com.trimble.r2m.ws.data.Configuration;
import com.trimble.r2m.ws.data.persistence.bean.Channel;
import com.trimble.r2m.ws.data.persistence.bean.ChannelValue;
import com.trimble.r2m.ws.data.persistence.bean.TmdsMonitoringData;

/**
 * Data access object for the channel value table.
 *
 */
public class ChannelValueDao extends NamedParameterJdbcDaoSupport {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChannelValueDao.class);

	private ResultSetExtractor<List<ChannelValue>> channelValueExtractor = new ResultSetExtractor<List<ChannelValue>>() {
		@Override
		public List<ChannelValue> extractData(ResultSet rs) throws SQLException, DataAccessException {
			List<ChannelValue> result = new ArrayList<>();
			try {
				while (rs.next()) {

					Integer speedValue = null;
					Boolean hcr = null;
					Boolean leftDoorEnable = null;
					Boolean rightDoorEnable = null;
					Boolean leftDoorClose = null;
					Boolean rightDoorClose = null;
					Boolean dir = null;
					Integer locationId = 0;
					String locationCode = null;

					Integer vehicleId = rs.getInt(1);
					Date timestamp = rs.getTimestamp(2);
					String trainID = rs.getString(3);                                  
					speedValue = rs.getInt(4);
					hcr = rs.getBoolean(5);                
					leftDoorEnable= rs.getBoolean(6); 
					rightDoorEnable= rs.getBoolean(7); 
					leftDoorClose= leftDoorEnable != null ? !leftDoorEnable : null; 
					rightDoorClose= rightDoorEnable != null ? !rightDoorEnable : null;
					dir= rs.getBoolean(8); 
					locationId= rs.getInt("locationId");
					locationCode = rs.getString("locationCode");

					result.add(new ChannelValue(vehicleId, timestamp, trainID, speedValue,
							hcr, leftDoorEnable, rightDoorEnable, leftDoorClose, rightDoorClose, dir,locationId,locationCode));
				}
			} catch(Exception e) {
				LOGGER.error("Exception: ",e.getMessage());
			}

			return result;
		}
	};

	private Channel speedChannel = null;

	private Channel hcrChannel = null;

	private Channel boggiePressure = null;

	private Channel leftDrEnable = null;

	private Channel rightDrEnable = null;

	private Channel drIntrlck = null;

	private Configuration conf;


	@Inject
	public ChannelValueDao(DataSourceProvider dsProvider, Configuration conf, ChannelCache channelCache) {
		this.setDataSource(dsProvider.getDatasource());
		this.conf = conf;

		String speedChannelHeader = conf.get().getString("appConfig.speed_chan_header");
		speedChannel = channelCache.getChannelsByHeader().get(speedChannelHeader);
		if (speedChannel == null) {
			LOGGER.error("No speed channel configured (header " + speedChannelHeader+ ")");
		}

		String hcrHeader = conf.get().getString("appConfig.hcr_chan_header");
		hcrChannel = channelCache.getChannelsByHeader().get(hcrHeader);
		if (hcrChannel == null) {
			LOGGER.info("No HCR channel configured (header " + hcrHeader+ ")");
		}

		String leftDoorEnable = conf.get().getString("appConfig.left_door_enable_chan_header");
		leftDrEnable = channelCache.getChannelsByHeader().get(leftDoorEnable);
		if (leftDrEnable == null) {
			LOGGER.info("No Left Door Enable channel configured (header " + leftDoorEnable + ")");
		}

		String rightDoorEnable = conf.get().getString("appConfig.right_door_enable_chan_header");
		rightDrEnable = channelCache.getChannelsByHeader().get(rightDoorEnable);
		if (rightDrEnable == null) {
			LOGGER.info("No Right Door Enable channel configured (header " + rightDoorEnable + ")");
		}

		String doorInterlock = conf.get().getString("appConfig.door_interlock_chan_header");
		drIntrlck = channelCache.getChannelsByHeader().get(doorInterlock);
		if (drIntrlck == null) {
			LOGGER.info("No DIR channel configured (header " + doorInterlock + ")");
		}

	}


	/**
	 * Return a map of the last channel value per unitId.
	 * It get the channel values received between startDate and stardate.
	 * If the last timestamp received is smaller than startdate or end date it is replaced by the last timestamp received
	 * @param startDate the start date 
	 * @param endDate the end date
	 * @return map of the latest channel values per vehicleId.
	 */
	public List<ChannelValue> getChannelValues(Date startDate, Date endDate) throws Exception {

		List<ChannelValue> channelValues = new ArrayList<>();
		StringBuilder query = new StringBuilder("SELECT DISTINCT cv.VehicleID, cv.timestamp, cv.TrainID ");
		try {
			if (speedChannel != null && !speedChannel.isBlob()) {
				query.append(", cv.").append(speedChannel.getColName());
			}
			if (hcrChannel != null && !hcrChannel.isBlob()) {
				query.append(", cv.").append(hcrChannel.getColName());
			}
			if (boggiePressure != null && !boggiePressure.isBlob()) {
				query.append(", cv.").append(boggiePressure.getColName());
			}
			if (leftDrEnable != null && !leftDrEnable.isBlob()) {
				query.append(", cv.").append(leftDrEnable.getColName());
			}
			if (rightDrEnable != null && !rightDrEnable.isBlob()) {
				query.append(", cv.").append(rightDrEnable.getColName());
			}  
			if (drIntrlck != null && !drIntrlck.isBlob()) {
				query.append(", cv.").append(drIntrlck.getColName());
			}
			query.append(", Location.ID as LocationId,LocationCode " + 
					"FROM ChannelValue cv INNER JOIN LocationArea " + 
					"ON cv.Col1 BETWEEN MinLatitude AND MaxLatitude " + 
					"INNER JOIN Location ON LocationID = Location.ID");

			query.append(" WHERE timestamp <= :endDate ");
			query.append(" AND timestamp > :startDate ");
			query.append("AND cv.Col2 BETWEEN MinLongitude AND MaxLongitude");
			query.append(" ORDER BY cv.timestamp");

			MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue("startDate", new Timestamp(startDate.getTime()));
			queryParams.addValue("endDate", new Timestamp(endDate.getTime()));

			channelValues =  this.getNamedParameterJdbcTemplate().query(query.toString(), queryParams, channelValueExtractor);
		} catch(DataAccessException e) {
			LOGGER.error("DataAccessException: ",e.getMessage());
		} catch(Exception ex) {
			LOGGER.error("Exception: ",ex.getMessage());
		}

		return channelValues;
	}

	/**
	 * Get the last received timestamp in channelvalue table.
	 * @return the more recent timestamp
	 */
	public Date getLastTimestampReceived() {
		Date lastTimestamp = null;
		try {
			String query = conf.getQuery("getMaxChannelValueTimestamp");
			lastTimestamp = this.getNamedParameterJdbcTemplate().queryForObject(query, Collections.emptyMap(), Timestamp.class);
		} catch(Exception e) {
			LOGGER.error("Exception: ",e.getMessage());
		}
		return lastTimestamp;
	}

	/**
	 * Get the first available received timestamp in channelvalue table.
	 * @return the least recent timestamp
	 */
	public Date getFirstTimestampReceived() {
		Date oldestTimestamp = null;
		try {
			String query = conf.getQuery("getMinChannelValueTimestamp");
			oldestTimestamp= this.getNamedParameterJdbcTemplate().queryForObject(query, Collections.emptyMap(), Timestamp.class);
		} catch(Exception e) {
			LOGGER.error("Exception: ",e.getMessage());
		}
		return oldestTimestamp;
	}

	private ResultSetExtractor<List<Integer>> locationIDExtractor = new ResultSetExtractor<List<Integer>>() {
		@Override
		public List<Integer> extractData(ResultSet rs) throws SQLException, DataAccessException {
			List<Integer> result = new ArrayList<>();
			try {                    
				while (rs.next()) {
					result.add(rs.getInt(1));
				}
			} catch(Exception e) {
				LOGGER.error("Exception:",e.getMessage());
			}
			return result;
		}
	};

	public boolean isStationPartOfService(String headcode,int locationId) {
		boolean isPartOfJourney = false;
		try {
			String query = conf.getQuery("getSectionPoint");
			MapSqlParameterSource queryParams = new MapSqlParameterSource();
			queryParams.addValue("headcode", headcode);
			queryParams.addValue("locationId", locationId);
			List<Integer> sectionPoints = this.getNamedParameterJdbcTemplate().query(query.toString(), queryParams, locationIDExtractor);
			if(sectionPoints != null && sectionPoints.size() > 0) {
				isPartOfJourney = true;
			}
		} catch(DataAccessException e) {
			LOGGER.error("Data Access Exception: ",e.getMessage());
		} catch(Exception ex) {
			LOGGER.error("Exception: ",ex.getMessage());
		}
		return isPartOfJourney;
	}

	private ResultSetExtractor<List<TmdsMonitoringData>> tmdsDataExtractor = new ResultSetExtractor<List<TmdsMonitoringData>>() {
		@Override
		public List<TmdsMonitoringData> extractData(ResultSet rs) throws SQLException, DataAccessException {
			List<TmdsMonitoringData> result = new ArrayList<TmdsMonitoringData>();
			try {                    
				while (rs.next()) {
					TmdsMonitoringData tmdsMonitoringData = new TmdsMonitoringData();
					tmdsMonitoringData.setVehicleId(rs.getInt("VehicleId"));
					tmdsMonitoringData.setLoadPressureAsp(rs.getDouble("LoadPressureApp"));
					tmdsMonitoringData.setTimestamp(rs.getTimestamp("Timestamp"));
					tmdsMonitoringData.setVehicleTypeId(rs.getInt("VehicleTypeId"));
					result.add(tmdsMonitoringData);
				} 
			} catch(Exception e) {
				LOGGER.error("Error while parsing TmdsMonitoringData ResultSet: "+e.getMessage() );
			}

			return result;
		}
	};

	/**
	 * Get the Tmds Monitoring Data for all vehicles in formation.
	 * @return list of TmdsMonitoringData
	 */

	public List<TmdsMonitoringData> getTmdsMonitoringData(Timestamp currentTimestamp, List<Integer>vehicleIds) {
		List<TmdsMonitoringData> tmdsMonitoringList = new ArrayList<TmdsMonitoringData>();
		try {
			String query = conf.getQuery("getTmdsChannelvalue");
			MapSqlParameterSource queryParams = new MapSqlParameterSource();            
			queryParams.addValue("currentTimestamp", currentTimestamp);
			queryParams.addValue("vehicleIds",vehicleIds);
			tmdsMonitoringList = this.getNamedParameterJdbcTemplate().query(query.toString(), queryParams, tmdsDataExtractor);
		} catch(DataAccessException e) {
			LOGGER.error("Data Access Exception: ",e.getMessage());
		} catch(Exception ex) {
			LOGGER.error("Exception:",ex.getMessage());
		}
		return tmdsMonitoringList;
	}

}

package com.trimble.r2m.ws.data.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import com.google.inject.Singleton;
import com.trimble.r2m.ws.data.Configuration;
import com.trimble.r2m.ws.data.persistence.bean.BogiePressureToPassengerLoadingConversion;
import com.trimble.r2m.ws.data.persistence.bean.Vehicle;

/**
 * Cache for the Vehicles.
 * 
 */
@Singleton
public class VehicleCache extends NamedParameterJdbcDaoSupport {
    
    private Map<Integer, Vehicle> vehicles = Collections.synchronizedMap(new HashMap<>());
    
    private Configuration conf;
    
    private ResultSetExtractor<Map<Integer, Vehicle>> vehicleExtractor = new ResultSetExtractor<Map<Integer, Vehicle>>() {
        @Override
        public Map<Integer, Vehicle> extractData(ResultSet rs) throws SQLException, DataAccessException {
            Map<Integer, Vehicle> result = new HashMap<>();
            while (rs.next()) {
                Integer vehicleId = rs.getInt(1);
                result.put(vehicleId, new Vehicle(vehicleId, rs.getString(2),rs.getString(3)));
            }
            return result;
        }
    };
    
    @Inject
    public VehicleCache(DataSourceProvider dsProvider, Configuration conf) {
        this.setDataSource(dsProvider.getDatasource());
        this.conf = conf;
        populateVehicleCache();       
    }
    
    /**
     * Retrieve the vehicles and populate cache.     * 
     */
    private void populateVehicleCache() {          
        String vehiclesQuery = conf.getQuery("getVehicles");
        this.vehicles = this.getNamedParameterJdbcTemplate().query(vehiclesQuery, vehicleExtractor);
    }
    
    /**
     * Return the vehicle Map pupulated in cache.     *
     */
    
    public Map<Integer, Vehicle>getVehicles() {
        return this.vehicles;
    }
}

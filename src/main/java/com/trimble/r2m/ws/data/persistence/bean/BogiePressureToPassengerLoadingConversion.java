package com.trimble.r2m.ws.data.persistence.bean;

public class BogiePressureToPassengerLoadingConversion {

	private Integer id;
	private Integer vehicleType;
	private Double pressureStart;
	private Double pressureEnd;
	private String passengerLoadingPercent;
	private Double passengerLoadingValue;
	private Boolean overload;
	
	public BogiePressureToPassengerLoadingConversion(Integer id, Integer vehicleType, Double pressureStart,
			Double pressureEnd, String passLoadPercent, Double passLoadValue, Boolean overload) {
		this.id = id;
		this.vehicleType = vehicleType;
		this.pressureStart = pressureStart;
		this.pressureEnd = pressureEnd;
		this.passengerLoadingPercent = passLoadPercent;
		this.passengerLoadingValue = passLoadValue;
		this.overload = overload;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(Integer vehicleType) {
		this.vehicleType = vehicleType;
	}
	public Double getPressureStart() {
		return pressureStart;
	}
	public void setPressureStart(Double pressureStart) {
		this.pressureStart = pressureStart;
	}
	public Double getPressureEnd() {
		return pressureEnd;
	}
	public void setPressureEnd(Double pressureEnd) {
		this.pressureEnd = pressureEnd;
	}
	public String getPassengerLoadingPercent() {
		return passengerLoadingPercent;
	}
	public void setPassengerLoadingPercent(String passengerLoadingPercent) {
		this.passengerLoadingPercent = passengerLoadingPercent;
	}
	public Boolean isOverload() {
		return overload;
	}
	public void setOverload(Boolean overload) {
		this.overload = overload;
	}

	public Double getPassengerLoadingValue() {
		return passengerLoadingValue;
	}

	public void setPassengerLoadingValue(Double passengerLoadingValue) {
		this.passengerLoadingValue = passengerLoadingValue;
	}
}

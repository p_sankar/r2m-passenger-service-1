package com.trimble.r2m.ws.data.provider;

import io.vertx.core.Vertx;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import com.trimble.r2m.ws.data.Configuration;
import com.trimble.r2m.ws.data.persistence.BoggiePressureConvCache;
import com.trimble.r2m.ws.data.persistence.ChannelValueDao;
import com.trimble.r2m.ws.data.persistence.ConfigurationDao;
import com.trimble.r2m.ws.data.persistence.DataSourceProvider;
import com.trimble.r2m.ws.data.persistence.FleetStatusCache;
import com.trimble.r2m.ws.data.persistence.PassengerLoadingDao;
import com.trimble.r2m.ws.data.persistence.Session;
import com.trimble.r2m.ws.data.persistence.VehicleCache;
import com.trimble.r2m.ws.data.persistence.bean.BogiePressureToPassengerLoadingConversion;
import com.trimble.r2m.ws.data.persistence.bean.ChannelValue;
import com.trimble.r2m.ws.data.persistence.bean.TmdsMonitoringData;
import com.trimble.r2m.ws.data.persistence.bean.Vehicle;

@Singleton
public class PassengerLoadingProviderImpl implements PassengerLoadingProvider{

	private static final Logger LOGGER = LoggerFactory.getLogger(PassengerLoadingProviderImpl.class);
    
    private ConfigurationDao configDao;
    
    private ChannelValueDao channelValueDao;
    
    private PassengerLoadingDao passengerLoadDao;
    
    private VehicleCache vehicleCache;
    
    private BoggiePressureConvCache boggiePressureCache;
    
    private FleetStatusCache fleetStatusCache;
    
    private Integer dataChunkInterval;
    
    private Long lastTimestampProcessed = null;
    
    private PlatformTransactionManager txManager;
    
    private List<Session> sessions = new ArrayList<>();
	
	@Inject
    public PassengerLoadingProviderImpl(ConfigurationDao configDao, ChannelValueDao channelValueDao,
			VehicleCache vehicleCache, DataSourceProvider dsProvider, Configuration conf,
			BoggiePressureConvCache boggiePressureCache, PassengerLoadingDao passengerLoadDao,
			FleetStatusCache fleetStatusCache) {
		this.configDao = configDao;
		this.channelValueDao = channelValueDao;
		this.vehicleCache = vehicleCache;
		this.boggiePressureCache = boggiePressureCache;
		this.fleetStatusCache = fleetStatusCache;
		this.txManager = new DataSourceTransactionManager(dsProvider.getDatasource());  
        dataChunkInterval = conf.get().getInt("appConfig.dataChunkInterval");
        this.passengerLoadDao = passengerLoadDao;
        
        String lastProcessed = configDao.getConfig("spectrum.passengerService.lastProcessedTimestamp");
		if(lastProcessed != null) {
			this.lastTimestampProcessed = Long.valueOf(lastProcessed);
		}
        
        createSessions();
	}	
        
	@Override
	public void computePassengerLoad(Vertx vertx) {

		if (lastTimestampProcessed == null) {
            // No passenger loading computed yet, starting from current time
            lastTimestampProcessed = System.currentTimeMillis();
    		configDao.insertConfig("spectrum.passengerService.lastProcessedTimestamp", lastTimestampProcessed.toString());
        }
		
		Date startDate = new Date(lastTimestampProcessed);
		Date lastTimestampReceived = channelValueDao.getLastTimestampReceived();
		Date endDate = new Date(lastTimestampProcessed+dataChunkInterval);
		Date oldestTimestampReceived = channelValueDao.getFirstTimestampReceived();
        // If channelValue is empty  lastTimestampReceived will be null
        if (lastTimestampReceived == null || lastTimestampReceived.before(startDate) 
        		|| oldestTimestampReceived == null) {
            // Start date is more recent than the last data received, don't process
            return;
        }      
        
        if (endDate.after(lastTimestampReceived)) {
            endDate = lastTimestampReceived;
        } else if (startDate.before(oldestTimestampReceived)) {
        	startDate = oldestTimestampReceived;
        	endDate = new Date(startDate.getTime() + dataChunkInterval);
        }
		try {
			List<ChannelValue> values = channelValueDao.getChannelValues(startDate, endDate);
			for(ChannelValue value : values) {
				Session sess = sessions.stream().filter(s -> s.getVehicleId() ==  value.getVehicleId()).findFirst().orElse(null);
				if(value.getTrainNumber() != null) {
					sess.setPartOfService(channelValueDao.isStationPartOfService(value.getTrainNumber(),value.getLocationId()));
				}				
				List<Session> formationSessions = new ArrayList<>();
				List<Integer> vehicles = fleetStatusCache.getVehiclesInFormation(value.getVehicleId());
				for(Integer vehicle: vehicles) {
					Optional<Session> formationVehicle = sessions.parallelStream().filter(s -> s.getVehicleId() == vehicle && s.getVehicleId() != value.getVehicleId()).findFirst();
					if(formationVehicle.isPresent()) {
						formationSessions.add(formationVehicle.get());
					}
				}
				sess.setFormationVehicles(formationSessions);
				sess.insertData(value);
			}
		} catch (Exception e) {			
			LOGGER.error("Exception in passenger load computation: "+e.getMessage());
		}
		
		lastTimestampProcessed = endDate.getTime();
		configDao.updateConfig("spectrum.passengerService.lastProcessedTimestamp",lastTimestampProcessed.toString());
	}
	
	protected void createSessions() {
		Map<Integer, Vehicle> vehicles = new HashMap<Integer, Vehicle>();
		vehicles = vehicleCache.getVehicles();
		for(int key: vehicles.keySet()){
			sessions.add(new Session(key));
		}
	}
	
	@Override
    public void insertPassengerLoadingData(int vehicleId, Timestamp currentTimestamp, String locationCode) {       
        //get all vehicles in formation based on leading vehicle
	    LOGGER.info("Fetching all vehicles in formation for leading vehicle:"+vehicleId);
	    try {
            List<Integer> vehicleList = fleetStatusCache.getVehiclesInFormation(vehicleId);
            if(!vehicleList.isEmpty()) {       
                //get data for all vehicles in formation corresponding to the timestamp
                LOGGER.info("Fetching recent data for each vehicle in formation from TmdsMonitoringData:"+vehicleId);
                List<TmdsMonitoringData> tmdsData = channelValueDao.getTmdsMonitoringData(currentTimestamp, vehicleList);
                //iterate for each vehicle data
                for (TmdsMonitoringData tmd : tmdsData) {
                    // find BoggiePressure object corresponding to current vehicle's type and pressure value
                    Optional<BogiePressureToPassengerLoadingConversion> bogiePressure = boggiePressureCache.getBoggiePressure()
                            .parallelStream()
                            .filter(f-> f.getVehicleType() == tmd.getVehicleTypeId() && 
                            (f.getPressureStart()<= tmd.getLoadPressureAsp() && f.getPressureEnd()>= tmd.getLoadPressureAsp()) || 
                            (f.getPressureStart()<= tmd.getLoadPressureAsp() && f.getPressureEnd() == 0.0)).findFirst();
                    if(bogiePressure.isPresent()) {            
                        //insert passenger loading data based on boggie pressure data retrieved
                        LOGGER.debug("Inserting passenger loading data for vehicle: "+tmd.getVehicleId());
                        passengerLoadDao.insertPassengerLoading(bogiePressure.get(),tmd.getVehicleId(), currentTimestamp, locationCode);
                    }
                }
            }
	    } catch (Exception e) {
	        LOGGER.error("Error while inserting passenger loading due to:"+e.getMessage());
	    }
    }

}

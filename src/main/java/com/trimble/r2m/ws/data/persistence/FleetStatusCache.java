package com.trimble.r2m.ws.data.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import com.google.inject.Singleton;
import com.trimble.r2m.ws.data.Configuration;
import com.trimble.r2m.ws.data.persistence.bean.FleetStatus;

/**
 * Cache for the Fleet Status.
 * 
 */
@Singleton
public class FleetStatusCache extends NamedParameterJdbcDaoSupport {
    
    private Map<Integer, FleetStatus> formations = Collections.synchronizedMap(new HashMap<>());
    
    private long lastRetrieved = 0;
    
    private long expiry;
    
    private Configuration conf;
    
    private ResultSetExtractor<Map<Integer, FleetStatus>> formationExtractor = new ResultSetExtractor<Map<Integer, FleetStatus>>() {
        @Override
        public Map<Integer, FleetStatus> extractData(ResultSet rs) throws SQLException, DataAccessException {
            Map<Integer, FleetStatus> result = new HashMap<>();
            while (rs.next()) {
                Integer vehicleId = rs.getInt(1);
                result.put(vehicleId, new FleetStatus(vehicleId, rs.getInt(2),rs.getInt(3)));
            }
            return result;
        }
    };
    
    @Inject
    public FleetStatusCache(DataSourceProvider dsProvider, Configuration conf) {
        this.setDataSource(dsProvider.getDatasource());
        this.conf = conf;
        this.expiry = conf.get().getLong("appConfig.formationCacheExpiry");
    }
    
    /**
     * Return the formations.
     * 
     * @return the formations by vehicle id.
     */
    protected Map<Integer, FleetStatus> getFormations() {
        long current = System.currentTimeMillis();
        if (formations == null || current - lastRetrieved > expiry) {
            
            String formationsQuery = conf.getQuery("getFormations");
            formations = this.getNamedParameterJdbcTemplate().query(formationsQuery, formationExtractor);
            lastRetrieved = current;
            
        }
        
        return formations;
    }
    
    /**
     * Return the vehicles in same formation.
     * 
     * @return the vehicles part of the same formation.
     */    
    public List<Integer> getVehiclesInFormation(Integer vehicleId) {
    	List<Integer> vehicles = new ArrayList<Integer>();

    	getFormations(); // This is done to refresh the cache if its expired
    	FleetStatus form = formations.get(vehicleId);
    	formations.entrySet().stream()
        .forEach(
               entry -> {
                    if (entry.getValue().getFleetFormationId()!= 0 && 
                    		entry.getValue().getFleetFormationId() == form.getFleetFormationId()) {
                        vehicles.add(entry.getKey());
                    } else if(entry.getValue().getUnitId()!= 0 && 
                    		entry.getValue().getUnitId() == form.getUnitId()) {
                        vehicles.add(entry.getKey());
                    }
                }
        );
    	
    	return vehicles;
    }
    
}


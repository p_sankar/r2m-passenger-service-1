package com.trimble.r2m.ws.data.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.binary.bean.ChannelSchema;
import com.nexala.spectrum.binary.db.rowMapper.ChannelSchemaRowMapper;
import com.trimble.r2m.ws.data.Configuration;
import com.trimble.r2m.ws.data.persistence.bean.Channel;

/**
 * Cache for the channel table.
 * @author BBaudry
 *
 */
@Singleton
public class ChannelCache extends NamedParameterJdbcDaoSupport {
    
    private Configuration conf;
    
    private ChannelSchemaRowMapper channelSchemaRowMapper;
    
    private Map<String, Channel> channelByHeader = Collections.synchronizedMap(new HashMap<>());
    
    private Map<Integer, ChannelSchema> channelSchemaById = Collections.synchronizedMap(new HashMap<>());
    
    private Boolean isBlob = null;
    
    private ResultSetExtractor<Map<String, Channel>> channelExtractor = new ResultSetExtractor<Map<String, Channel>>() {
        @Override
        public Map<String, Channel> extractData(ResultSet rs) throws SQLException, DataAccessException {
            Map<String, Channel> result = new HashMap<>();
            while (rs.next()) {
                String header = rs.getString(1);
                Channel c = new Channel(header, "Col"+rs.getInt(2), rs.getString(3), isBlob);
                result.put(header, c);
            }
            return result;
        }
    };
    
    @Inject
    public ChannelCache(DataSourceProvider dsProvider, Configuration conf, ChannelSchemaRowMapper channelSchemaRowMapper) {
        this.setDataSource(dsProvider.getDatasource());
        this.conf = conf;
        this.channelSchemaRowMapper = channelSchemaRowMapper;
        isBlob = conf.get().getBoolean(Configuration.CONFIG_IS_BLOB);
    }
    
    /**
     * Return the list of column name by the header name.
     * @return the list of column name by the header name.
     */
    public Map<String, Channel> getChannelsByHeader() {       
        if (channelByHeader.isEmpty()) {
            String channelsQuery = conf.getQuery("getChannels");
            channelByHeader = this.getNamedParameterJdbcTemplate().query(channelsQuery, channelExtractor);
        }        
        return channelByHeader;        
    }
    
    
    /**
     * Returns a list of schema_version-ChannelSchema pairs
     * @return
     * @throws SQLException 
     */
    public Map<Integer, ChannelSchema> getChannelSchema() throws SQLException {
        
        if (Boolean.TRUE.equals(isBlob) && channelSchemaById.isEmpty()) {
            String query = conf.getQuery("getChannelSchema");
            List<ChannelSchema> schemaList = new ArrayList<ChannelSchema>();

            schemaList = (List<ChannelSchema>) this.getNamedParameterJdbcTemplate().query(query, channelSchemaRowMapper.getJdbcRowMapper());
            
            for (ChannelSchema schema : schemaList) {
                channelSchemaById.put(schema.getSchemaId(), schema);
            }
        }
        

        return channelSchemaById;
    }

}

package com.trimble.r2m.ws.data.service;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trimble.r2m.ws.data.Configuration;
import com.trimble.r2m.ws.data.provider.PassengerLoadingProvider;

import io.vertx.core.AbstractVerticle;

public class PassengerLoadingService extends AbstractVerticle {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PassengerLoadingService.class);
	
	private final Configuration conf;

	private final PassengerLoadingProvider passengerProvider;
	
	private long timerId;
	
	
	@Inject
	public PassengerLoadingService(Configuration conf, PassengerLoadingProvider passengerProvider) {
		this.conf = conf;
		this.passengerProvider = passengerProvider;
	}
		
	@Override
	public void start() {
	    int interval = conf.get().getInt("appConfig.scanInterval");
	    LOGGER.info("Service is starting and will run every "+interval+" ms");
	    timerId = vertx.setPeriodic(interval, id -> {
	        
	        long start = System.currentTimeMillis();
	        
	        passengerProvider.computePassengerLoad(vertx);
	        
	        long end = System.currentTimeMillis();
	        
	        if (end-start > interval) {
	            LOGGER.warn("Slow passenger Loading computation : "+(end-start)+ " ms");
	        }
	        
        });
		
		
	}
	
	
	@Override
    public void stop() throws Exception {
        LOGGER.info("passenger service stopping...");
        vertx.cancelTimer(timerId);
    }

}

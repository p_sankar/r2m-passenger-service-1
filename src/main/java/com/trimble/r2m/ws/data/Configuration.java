package com.trimble.r2m.ws.data;

import com.google.inject.Singleton;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

@Singleton
public class Configuration {
    
    public static final String CONFIG_PATH = "fileService.configPath";
    public static final String FILE_PATH = "fileService.filesPath";
    public static final String CONFIG_DOWNLOAD_PATH = "fileService.configDownloadPath";
    public static final String CONFIG_IS_BLOB = "appConfig.blobEnabled";
    
    
    
	private final Config conf = ConfigFactory.load();

	public Config get() {
		return conf;
	}
	
	
	public String getQuery(String queryId) {
        return conf.getString("query." + queryId);
    }

}

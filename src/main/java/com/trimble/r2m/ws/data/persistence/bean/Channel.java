package com.trimble.r2m.ws.data.persistence.bean;

/**
 * Represent a channel.
 * @author BBaudry
 *
 */
public class Channel {
    
    private String header;
    
    private String colName;
    
    private String datatype;
    
    private Boolean blob;
    
    public Channel(String header, String colName, String datatype, Boolean isBlob) {
        this.header = header;
        this.colName = colName;
        this.datatype = datatype;
        this.blob = isBlob;
    }

    public Channel(String header, String colName, String datatype) {
        this(header,colName,datatype, false);
        
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public String getDatatype() {
        return datatype;
    }

    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    public Boolean isBlob() {
        return blob;
    }

    public void setBlob(Boolean blob) {
        this.blob = blob;
    }

}

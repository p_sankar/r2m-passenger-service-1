package com.trimble.r2m.ws.data.persistence;

import javax.sql.DataSource;

/**
 * Provide the datasource for the application
 * @author BBaudry
 *
 */
public interface DataSourceProvider {
    
    /**
     * Return the datasource.
     * @return the datasource
     */
    public DataSource getDatasource();

}

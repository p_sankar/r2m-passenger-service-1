package com.trimble.r2m.ws.data.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import com.google.inject.Inject;
import com.trimble.r2m.ws.data.Configuration;
import com.trimble.r2m.ws.data.persistence.bean.ConfigurationBean;

/**
 * Data access object for the configuration table
 * @author BBaudry
 *
 */
public class ConfigurationDao extends NamedParameterJdbcDaoSupport {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationDao.class);
    
    private ResultSetExtractor<List<ConfigurationBean>> configExtractor = new ResultSetExtractor<List<ConfigurationBean>>() {
        @Override
        public List<ConfigurationBean> extractData(ResultSet rs) throws SQLException, DataAccessException {
            List<ConfigurationBean> result = new ArrayList<>();
            
            while (rs.next()) {
                result.add(new ConfigurationBean(rs.getString(1), rs.getString(2)));
            }
            
            return result;
        }
    };
    
    private Configuration conf;
    
    @Inject
    public ConfigurationDao(DataSourceProvider dsProvider, Configuration conf) {
        this.setDataSource(dsProvider.getDatasource());
        this.conf = conf;
    }
    
    
    /**
     * Gets the property from Configuration table
     * @param name of the Property
     * @return Return the property value
     */
    public String getConfig(String name) {
        String result = null;
        String query = conf.getQuery("getConfig");
        MapSqlParameterSource queryParams = new MapSqlParameterSource();
        queryParams.addValue("name", name);
        List<ConfigurationBean> configs = this.getNamedParameterJdbcTemplate().query(query, queryParams, configExtractor);
        
        if (configs.size() == 1) {
            ConfigurationBean configBean = configs.get(0);
            String propertyValue = configBean.getPropertyValue();
            try {
                result = propertyValue;
            } catch (NumberFormatException e) {
                LOGGER.error("Can't parse property from Config table", e);
            }
        }
        
        return result;
    }
    
    /**
     * Update any property's value in the Configuration table in database
     * @param name of the Property
     * @param value that needs to be set
     */
    public void updateConfig(String name, String value) {
        
        if (value == null) {
            return;
        }
        
        String query = conf.getQuery("updateConfig");
        
        MapSqlParameterSource queryParams = new MapSqlParameterSource();
        queryParams.addValue("name", name);
        queryParams.addValue("val", value);
        
        this.getNamedParameterJdbcTemplate().update(query.toString(), queryParams);
        
    }
    
    /**
     * Insert a property and its value into Configuration table in the database
     * @param name of the Property
     * @param value of the Property
     */
    public void insertConfig(String name, String value) {
        String query = conf.getQuery("insertConfig");
        
        MapSqlParameterSource queryParams = new MapSqlParameterSource();
        queryParams.addValue("name", name);
        queryParams.addValue("val", value);
        
        this.getNamedParameterJdbcTemplate().update(query.toString(), queryParams);
    }
   
}

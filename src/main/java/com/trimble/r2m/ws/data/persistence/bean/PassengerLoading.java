package com.trimble.r2m.ws.data.persistence.bean;
import java.util.Date;

public class PassengerLoading {

	private Long passengerLoadingId;
	private Integer vehicleId;
	private String passengerLoadingPercent;
	private Date ValidFrom;
	private Date ValidTo;
	private String departingStation;
	private Double passengerLoadingValue;
	private Boolean overload;
	
	public Long getPassengerLoadingId() {
		return passengerLoadingId;
	}
	public void setPassengerLoadingId(Long passengerLoadingId) {
		this.passengerLoadingId = passengerLoadingId;
	}
	public Integer getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(Integer vehicleId) {
		this.vehicleId = vehicleId;
	}
	public String getPassengerLoadingPercent() {
		return passengerLoadingPercent;
	}
	public void setPassengerLoadingPercent(String passengerLoadingPercent) {
		this.passengerLoadingPercent = passengerLoadingPercent;
	}
	public Date getValidFrom() {
		return ValidFrom;
	}
	public void setValidFrom(Date validFrom) {
		ValidFrom = validFrom;
	}
	public Date getValidTo() {
		return ValidTo;
	}
	public void setValidTo(Date validTo) {
		ValidTo = validTo;
	}
	public String getDepartingStation() {
		return departingStation;
	}
	public void setDepartingStation(String departingStation) {
		this.departingStation = departingStation;
	}
	public Double getPassengerLoadingValue() {
		return passengerLoadingValue;
	}
	public void setPassengerLoadingValue(Double passengerLoadingValue) {
		this.passengerLoadingValue = passengerLoadingValue;
	}
	public Boolean getOverload() {
		return overload;
	}
	public void setOverload(Boolean overload) {
		this.overload = overload;
	}
}

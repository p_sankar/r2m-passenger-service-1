package com.trimble.r2m.ws.data.persistence.bean;

import java.util.Date;

/**
 * Bean for TmdsmonitoringData.
 * @author psankar
 *
 */
public class TmdsMonitoringData {
    int vehicleId;
    double loadPressureAsp;
    Date timestamp;
    int vehicleTypeId;
    
    public int getVehicleId() {
        return vehicleId;
    }
    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }
    public double getLoadPressureAsp() {
        return loadPressureAsp;
    }
    public void setLoadPressureAsp(double loadPressureAsp) {
        this.loadPressureAsp = loadPressureAsp;
    }
    public Date getTimestamp() {
        return timestamp;
    }
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
    public int getVehicleTypeId() {
        return vehicleTypeId;
    }
    public void setVehicleTypeId(int vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }
    

}

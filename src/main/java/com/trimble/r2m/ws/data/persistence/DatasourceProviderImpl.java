package com.trimble.r2m.ws.data.persistence;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.trimble.r2m.ws.data.Configuration;

@Singleton
public class DatasourceProviderImpl implements DataSourceProvider {
    
    private final DataSource ds;
    
    @Inject
    public DatasourceProviderImpl(Configuration config) {
        BasicDataSource bds = new BasicDataSource();
        bds.setDriverClassName(config.get().getString("dbConfig.driverClassName"));
        bds.setUrl(config.get().getString("dbConfig.url"));
        bds.setUsername(config.get().getString("dbConfig.user"));
        bds.setPassword(config.get().getString("dbConfig.password"));
        bds.setValidationQuery("select 1");
        ds = bds;
    }

    @Override
    public DataSource getDatasource() {
        return ds;
    }

}

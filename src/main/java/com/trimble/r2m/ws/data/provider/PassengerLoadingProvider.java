package com.trimble.r2m.ws.data.provider;

import java.sql.Timestamp;

import io.vertx.core.Vertx;

public interface PassengerLoadingProvider {

	/**
     * Compute the passenger loading from the channelValue table and update the db accordingly.
     * This will update the passenger loading table by referring the boggiePressureConversion table
     * @param vertx 
     */
    public void computePassengerLoad(Vertx vertx);
    
    /**
     * Insert the passenger loading data from the TmdsMonitoringData table and update the PassengerLoadingTable accordingly.
     * @param vehicleId -id of vehicle in TmdsmonitoringData table
     * @param timestamp - timestamp at which TmdsMonitroingTabe is probed every x secs
     * @param locationCode - loccation code computed dynamically using the gps cordinates of train at the time of probe  
     */    
    public void insertPassengerLoadingData(int vehicleId,Timestamp timestamp, String locationCode);
}

package com.trimble.r2m.ws.data.persistence.bean;

/**
 * Unit bean for the persistence layer.
 * @author BBaudry
 *
 */
public class Unit {
    
    private Integer unitId;
    
    private String unitNumber;

    public Unit(Integer unitId, String unitNumber) {
        this.unitId = unitId;
        this.unitNumber = unitNumber;
    }

    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    public String getUnitNumber() {
        return unitNumber;
    }

    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }

}

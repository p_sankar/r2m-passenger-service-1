package com.trimble.r2m.ws.data.persistence.bean;

public enum VehicleState {
InMotion,
Stopped, 
PartOfService, 
OneDoorEnabled, 
OneDoorClosed, 
AllDoorsClosed
}

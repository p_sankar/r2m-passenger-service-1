package com.trimble.r2m.ws.data.module;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.trimble.r2m.ws.data.Configuration;
import com.trimble.r2m.ws.data.persistence.DataSourceProvider;
import com.trimble.r2m.ws.data.persistence.DatasourceProviderImpl;
import com.trimble.r2m.ws.data.provider.PassengerLoadingProvider;
import com.trimble.r2m.ws.data.provider.PassengerLoadingProviderImpl;

public class DataWsModule extends AbstractModule {

	@Override
	protected void configure() {
	    binder().bind(Configuration.class);
        binder().bind(PassengerLoadingProvider.class).to(PassengerLoadingProviderImpl.class).in(Scopes.SINGLETON);
        
        bind(DataSourceProvider.class).to(DatasourceProviderImpl.class);
	}
}

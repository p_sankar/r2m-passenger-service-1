package com.trimble.r2m.ws.data.persistence.bean;

import java.util.Date;

/**
 * Bean for channelValue.
 * @author BBaudry
 *
 */
public class ChannelValue {
    
    private Integer vehicleId;
    
    private Date timestamp;
    
    private String trainNumber;
    
    private Integer speed;
    
    private Boolean hcr;
    
    private Boolean leftDoorEnable;
    
    private Boolean rightDoorEnable;
    
    private Boolean leftDoorClose;
    
    private Boolean rightDoorClose;
    
    public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	private Boolean dir; 
    
    private Integer locationId;
    
    private String locationCode;

    public String getLocationCode() {
		return locationCode;
	}

	public Integer getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(Integer vehicleId) {
		this.vehicleId = vehicleId;
	}

	public Integer getSpeed() {
		return speed;
	}

	public void setSpeed(Integer speed) {
		this.speed = speed;
	}

	public Boolean getHcr() {
		return hcr;
	}

	public void setHcr(Boolean hcr) {
		this.hcr = hcr;
	}

	public Boolean getLeftDoorEnable() {
		return leftDoorEnable;
	}

	public void setLeftDoorEnable(Boolean leftDoorEnable) {
		this.leftDoorEnable = leftDoorEnable;
	}

	public Boolean getRightDoorEnable() {
		return rightDoorEnable;
	}

	public void setRightDoorEnable(Boolean rightDoorEnable) {
		this.rightDoorEnable = rightDoorEnable;
	}

	public Boolean getLeftDoorClose() {
		return leftDoorClose;
	}

	public void setLeftDoorClose(Boolean leftDoorClose) {
		this.leftDoorClose = leftDoorClose;
	}

	public Boolean getRightDoorClose() {
		return rightDoorClose;
	}

	public void setRightDoorClose(Boolean rightDoorClose) {
		this.rightDoorClose = rightDoorClose;
	}

	public Boolean getDir() {
		return dir;
	}

	public void setDir(Boolean dir) {
		this.dir = dir;
	}

	public ChannelValue(Integer unitId, Date timestamp, String trainNumber,
			Integer speed, Boolean hcr, Boolean leftDoorEnable, Boolean rightDoorEnable,
			Boolean leftDoorClose, Boolean rightDoorClose, Boolean dir, Integer locationId,
			String locationCode) {
        this.vehicleId = unitId;
        this.timestamp = timestamp;
        this.trainNumber = trainNumber;
        this.speed = speed;
        this.hcr = hcr;
        this.leftDoorEnable = leftDoorEnable;
        this.rightDoorEnable = rightDoorEnable;
        this.leftDoorClose = leftDoorClose;
        this.rightDoorClose = rightDoorClose;
        this.dir = dir;
        this.locationId = locationId;
        this.locationCode = locationCode;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }



}

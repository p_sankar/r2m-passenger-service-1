package com.trimble.r2m.ws.data.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import com.google.inject.Singleton;
import com.trimble.r2m.ws.data.Configuration;
import com.trimble.r2m.ws.data.persistence.bean.BogiePressureToPassengerLoadingConversion;

/**
* Cache for the BoggiePressureConvertion to passenger load.
* 
*/
@Singleton
public class BoggiePressureConvCache extends NamedParameterJdbcDaoSupport{
    
    private List<BogiePressureToPassengerLoadingConversion> boggiePressureConversion = Collections.synchronizedList(new ArrayList<>());   
    private Configuration conf;    
    
    private ResultSetExtractor<List<BogiePressureToPassengerLoadingConversion>> pressureConversionExtractor = new ResultSetExtractor<List<BogiePressureToPassengerLoadingConversion>>() {
        @Override
        public List<BogiePressureToPassengerLoadingConversion> extractData(ResultSet rs) throws SQLException, DataAccessException {
            List<BogiePressureToPassengerLoadingConversion> result = new ArrayList<>();
            while (rs.next()) {               
                result.add( new BogiePressureToPassengerLoadingConversion(rs.getInt("Id"), rs.getInt("vehicleType"), rs.getDouble("pressureStart"),
                		rs.getDouble("pressureEnd"), rs.getString("PassengerLoadingPercent"), rs.getDouble("PassengerLoadingValue"), rs.getBoolean("Overload")));
            }
            return result;
        }
    };
    
    @Inject
    public BoggiePressureConvCache(DataSourceProvider dsProvider, Configuration conf) {
        this.setDataSource(dsProvider.getDatasource());
        this.conf = conf;
        populateBoggiePressureCache();
    }
    
    /**
     * Retrieve the boggie pressure conversion values as list and populate in cache.     * 
     *
     */
    private void populateBoggiePressureCache() {                  
        String channelsQuery = conf.getQuery("getBogiePressureToPassengerConversions");
        this.boggiePressureConversion= this.getNamedParameterJdbcTemplate().query(channelsQuery, pressureConversionExtractor);      
    }
    
    /**
     * Return the boggie pressure conversion values as list from cache.     * 
     *
     */
    
    public List<BogiePressureToPassengerLoadingConversion>getBoggiePressure() {
        return this.boggiePressureConversion;
    }
}
package com.trimble.r2m.ws.data.persistence;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.trimble.r2m.ws.data.module.DataWsModule;
import com.trimble.r2m.ws.data.persistence.bean.ChannelValue;
import com.trimble.r2m.ws.data.persistence.bean.VehicleState;
import com.trimble.r2m.ws.data.provider.PassengerLoadingProvider;


public class Session { 

	private final int vehicleId; 
	
	private String locationCode; 

	private VehicleState state = VehicleState.InMotion; // the step

	public VehicleState getState() {
		return state;
	}
	
	Injector injector = Guice.createInjector(new DataWsModule());

	private List<Session> formationVehicles = new ArrayList<>(); 

	private boolean isLeadVehicle;

	public boolean isLeadVehicle() {
		return isLeadVehicle;
	}

	private Boolean isOneDoorOpen; 

	private Boolean isAllDoorClosed; 
	
	private Boolean isCloseActivated;
	
	public Boolean getIsCloseActivated() {
		return isCloseActivated;
	}

	private Boolean isStopped;

	public Boolean getIsStopped() {
		return isStopped;
	}

	private boolean isPartOfService;

	public boolean isPartOfService() {
		return isPartOfService;
	}

	public void setPartOfService(boolean isPartOfService) {
		this.isPartOfService = isPartOfService;
	}

	public Session(int vehicleId) { 
		this.vehicleId = vehicleId; 
	} 

	public List<Session> getFormationVehicles() {
		return formationVehicles;
	}

	public void setFormationVehicles(List<Session> formationVehicles) {
		this.formationVehicles = formationVehicles;
	}

	public int getVehicleId() {
		return vehicleId;
	}

	public void insertData(ChannelValue c) 	{
		if(c.getSpeed() == 0) {
			isStopped = true;
		}
		else {
			isStopped = false;
		}
		locationCode = c.getLocationCode();
		// These two conditions are yet to be finalized need some else logic to reset the flags
		if((c.getRightDoorEnable() != null  && c.getRightDoorEnable())
			|| (c.getLeftDoorEnable() != null && c.getLeftDoorEnable())) {
			isOneDoorOpen = true;
		}
		if((c.getLeftDoorClose() != null && c.getLeftDoorClose()) 
				|| (c.getRightDoorClose() != null && c.getRightDoorClose())) {
			// Any train door close switch is activated
			isCloseActivated = true;
		}
		if(c.getDir() != null) {
			isAllDoorClosed = c.getDir(); // All doors in this vehicle are closed
		}
		if(c.getHcr() != null && c.getHcr()) {
			isLeadVehicle = true;
			computeStep();
		}
		else {
			state = VehicleState.InMotion; 
			// this is to reset state in case the vehicle was leading previously
			isLeadVehicle = false;
			Optional<Session> lead = formationVehicles.stream().filter(v -> v.isLeadVehicle()).findFirst();
			if(lead.isPresent()) {
				lead.get().computeStep();
			}
		}
	}
	
	public void computeStep() { 		
		if(isStopped) {
			switch(state) {
				case InMotion:
				case Stopped:
					state = VehicleState.Stopped;
					if(isLeadVehicle && isPartOfService) {
						// This passes only for the leading vehicle
						// And the vehicle which has train number populated 
						// Also the station is part of the journey
						state = VehicleState.PartOfService;
					}
					break;
				case PartOfService: 
					if((isOneDoorOpen != null && isOneDoorOpen) || formationVehicles.stream().anyMatch(v -> v.isOneDoorOpen() != null 
					&& v.isOneDoorOpen())) {
						state = VehicleState.OneDoorEnabled;
						//Any train door(s) enabled on any of the vehicles in the formation.
					}
					break;
				case OneDoorEnabled:
					if((isCloseActivated != null && isCloseActivated) || formationVehicles.stream().anyMatch(v -> v.getIsCloseActivated() != null 
					&& v.getIsCloseActivated())) {
						// Any train door close switch is activated in any vehicle in the formation
						state = VehicleState.OneDoorClosed;
					}
					break;
				case OneDoorClosed:
					if(isAllDoorClosed != null && isAllDoorClosed == true && 
					formationVehicles.stream().allMatch(v -> v.isAllDoorClosed() == null 
					|| v.isAllDoorClosed() == true)) {
						// We can start the timer here
						state = VehicleState.AllDoorsClosed;
						long retryDate = System.currentTimeMillis();
				        int sec = 1;
				        Calendar cal = Calendar.getInstance();
				        cal.setTimeInMillis(retryDate);
				        cal.add(Calendar.SECOND, sec);
				        Timestamp pressureTimestamp = new Timestamp(cal.getTime().getTime());

						Timer pressureTimer = new Timer();						
						TimerTask calculatePassengerLoading = new TimerTask() {
							@Override
							public void run() {
								//invoke the method to compute passenger loadings
								// pass the timestamp as pressureTimestamp
								//vehicle ID and locationId for now
								injector.getInstance(PassengerLoadingProvider.class).
								insertPassengerLoadingData(vehicleId, pressureTimestamp, locationCode);
							}
							
						};
						pressureTimer.schedule(calculatePassengerLoading, 1000);
					}
					break;
				default:
					state = VehicleState.InMotion;
					
			}
		}	
		else {
			//This is to ensure that if speed != 0, return to step 1 from any state
			state = VehicleState.InMotion;
		}
	}

	public Boolean isOneDoorOpen() { 
		return isOneDoorOpen; 
	} 

	public Boolean isAllDoorClosed() { 
		return isAllDoorClosed; 
	} 

}
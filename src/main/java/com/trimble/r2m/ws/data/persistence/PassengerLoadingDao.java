package com.trimble.r2m.ws.data.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.trimble.r2m.ws.data.Configuration;
import com.trimble.r2m.ws.data.persistence.bean.BogiePressureToPassengerLoadingConversion;
import com.trimble.r2m.ws.data.persistence.bean.PassengerLoading;

@Singleton
public class PassengerLoadingDao extends NamedParameterJdbcDaoSupport{

    private static final Logger LOGGER = LoggerFactory.getLogger(PassengerLoadingDao.class);

    private Configuration conf;

    private ResultSetExtractor<List<PassengerLoading>> passengerLoadExtractor = new ResultSetExtractor<List<PassengerLoading>>() {
        @Override
        public List<PassengerLoading> extractData(ResultSet rs) throws SQLException, DataAccessException {
            List<PassengerLoading> result = new ArrayList<>();
            while (rs.next()) {
                PassengerLoading current = new PassengerLoading();
                current.setPassengerLoadingId(rs.getLong(1));
                current.setVehicleId(rs.getInt(2));
                current.setPassengerLoadingPercent(rs.getString(3));
                current.setValidFrom(rs.getTimestamp(4));
                current.setValidTo(rs.getTimestamp(5));
                current.setDepartingStation(rs.getString(6));	
                current.setPassengerLoadingValue(rs.getDouble(7));
                current.setOverload(rs.getBoolean(8));
                result.add(current);
            }
            return result;
        }
    };

    @Inject
    public PassengerLoadingDao(DataSourceProvider dsProvider, Configuration conf) {
        this.setDataSource(dsProvider.getDatasource());
        this.conf = conf;
    }

    /**
     * Return all the PassengerLoading
     * @return all the PassengerLoading
     */
    public List<PassengerLoading> getPassengerLoading() {
        String channelsQuery = conf.getQuery("getPassengerLoading");
        return this.getNamedParameterJdbcTemplate().query(channelsQuery, passengerLoadExtractor);
    }


    /**
     * Insert a passengerLoading into the DB
     * @param boggiePressure Obj, vehicleId, Timestamp 
     * @return void
     */  
    public void insertPassengerLoading(BogiePressureToPassengerLoadingConversion bogiePressure, int vehicleId, Timestamp currentTimestamp, String locationCode) { 
        try {
            String query = conf.getQuery("insertPassengerLoadingData");
            MapSqlParameterSource queryParams = new MapSqlParameterSource();
            queryParams.addValue("vehicleId", vehicleId);
            queryParams.addValue("loadingPercent", bogiePressure.getPassengerLoadingPercent());
            queryParams.addValue("loadingValue", bogiePressure.getPassengerLoadingValue());
            queryParams.addValue("currentTimestamp", currentTimestamp);            
            queryParams.addValue("overLoad", bogiePressure.isOverload());
            queryParams.addValue("departingStation", locationCode);        
            this.getNamedParameterJdbcTemplate().update(query.toString(), queryParams);
            LOGGER.info ("Inserted passenger loading data for vehicle: "+vehicleId);
        } catch(Exception e) {
            LOGGER.error("Error while insert in PassengerLoadingDao",e.getMessage());
        }
    }
}

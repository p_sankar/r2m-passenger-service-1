package com.trimble.r2m.ws.data.persistence.bean;

public class FleetStatus {

	private Integer vehicleId;
	private Integer fleetFormationId;
	private Integer unitId;
	
	public Integer getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(Integer vehicleId) {
		this.vehicleId = vehicleId;
	}

	public Integer getFleetFormationId() {
		return fleetFormationId;
	}

	public void setFleetFormationId(Integer fleetFormationId) {
		this.fleetFormationId = fleetFormationId;
	}

	public Integer getUnitId() {
		return unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public FleetStatus(Integer vehicleId,Integer formationId,Integer unitId) {
		this.vehicleId = vehicleId;
		this.fleetFormationId = formationId;
		this.unitId = unitId;
	}
}

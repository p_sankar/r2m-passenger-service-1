package com.trimble.r2m.ws.data.persistence.bean;

/**
 * Vehicle bean for the persistence layer.
 * @author BBaudry
 *
 */
public class Vehicle {
    
    private Integer vehicleId;
    
    private String vehicleNumber;
    
    private String vehicleType;
    
    public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getHeadcode() {
		return headcode;
	}

	public void setHeadcode(String headcode) {
		this.headcode = headcode;
	}

	public String getTrainNumber() {
		return trainNumber;
	}

	public void setTrainNumber(String trainNumber) {
		this.trainNumber = trainNumber;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	private String headcode;
    
    private String trainNumber;
    
    private String location;

    public Vehicle(Integer vehicleId, String vehicleNumber, String type) {
        this.vehicleId = vehicleId;
        this.vehicleNumber = vehicleNumber;
        this.vehicleType = type;
    }

    public Integer getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Integer vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

}

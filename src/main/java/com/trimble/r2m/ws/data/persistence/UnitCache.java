package com.trimble.r2m.ws.data.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import com.google.inject.Singleton;
import com.trimble.r2m.ws.data.Configuration;
import com.trimble.r2m.ws.data.persistence.bean.Unit;

/**
 * Cache for the Units.
 * @author BBaudry
 *
 */
@Singleton
public class UnitCache extends NamedParameterJdbcDaoSupport {
    
    private Map<Integer, Unit> units = Collections.synchronizedMap(new HashMap<>());
    
    private long lastRetrieved = 0;
    
    private long expiry;
    
    private Configuration conf;
    
    private ResultSetExtractor<Map<Integer, Unit>> unitExtractor = new ResultSetExtractor<Map<Integer, Unit>>() {
        @Override
        public Map<Integer, Unit> extractData(ResultSet rs) throws SQLException, DataAccessException {
            Map<Integer, Unit> result = new HashMap<>();
            while (rs.next()) {
                Integer unitId = rs.getInt(1);
                result.put(unitId, new Unit(unitId, rs.getString(2)));
            }
            return result;
        }
    };
    
    @Inject
    public UnitCache(DataSourceProvider dsProvider, Configuration conf) {
        this.setDataSource(dsProvider.getDatasource());
        this.conf = conf;
        this.expiry = conf.get().getLong("appConfig.unitCacheExpiry");
    }
    
    /**
     * Return the units.
     * 
     * @return the units by unit id.
     */
    public Map<Integer, Unit> getUnits() {
        long current = System.currentTimeMillis();
        if (units == null || current - lastRetrieved > expiry) {
            
            String channelsQuery = conf.getQuery("getUnits");
            units = this.getNamedParameterJdbcTemplate().query(channelsQuery, unitExtractor);
            lastRetrieved = current;
            
        }
        
        return units;
    }
    
}
